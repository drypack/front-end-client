/*eslint-env es6*/

(function () {

  'use strict';

  angular
    .module('app')
    .controller('MenuController', MenuController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function MenuController($mdSidenav, $state, $mdColors, Global, $mdMedia, MenuStates) {
    var vm = this;

    vm.open = open;
    vm.openMenuOrRedirectToState = openMenuOrRedirectToState;
    vm.isOpen = isOpen;
    vm.close = close;

    activate();

    function activate() {
      vm.currentMenuItem = Global.homeState;

      // Array with the menu items that are declared in the MenuStates constant
      vm.itemsMenu = MenuStates;

       /**
       * Object that fills the ng-style color and style attributes
       */
      vm.navStyle = {
        top: {
          'border-bottom': '1px solid ' + getColor('primary'),
          'background-image': '-webkit-linear-gradient(top, ' + getColor('primary-500') + ', ' + getColor('primary-800') + ')'
        },
        content: {
          'background-color': getColor('primary-800')
        },
        menuBar: {
          'background-color': getColor('primary-800')
        },
        textColor: {
          color: '#FFF'
        },
        lineBottom: {
          'border-bottom': '1px solid ' + getColor('primary-400')
        }
      }
    }


    function open() {
      $mdSidenav('left').toggle();
    }
    function isOpen() {
      var sidenav = $mdSidenav('left');

      return sidenav.isLockedOpen() || sidenav.isOpen();
    }

   /**
     * Show the sub menu items or redirect to the item state if it has no subitems
     */
    function openMenuOrRedirectToState($mdMenu, ev, item) {
      if (angular.isDefined(item.subItems) && item.subItems.length > 0) {
        $mdMenu.open(ev);
      } else {
        vm.currentMenuItem = item.state;
        if (!$mdMedia('gt-sm')) {
          $mdSidenav('left').close();
        }
        $state.go(item.state);
      }
    }

    function getColor(colorPalettes) {
      return $mdColors.getThemeColor(colorPalettes);
    }
  }
})();
