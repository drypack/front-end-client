(function() {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for the preferences state
   *
   * @param {object} $stateProvider
   * @param {object} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.preferences', {
        url: '/preferences',
        templateUrl: Global.clientPath + '/preferences/preferences.html',
        controller: 'PreferencesController as preferencesCtrl',
        data: { }
      });
  }
}());
