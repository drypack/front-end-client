/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  angular
    .module('app')
    .constant('pt-BR.i18n.views', {
      breadcrumbs: {
        'not-authorized': 'Não autorizado',
        project: 'Projeto',
        preferences: 'Preferências',
        home: 'Página inicial'
      },
      titles: {
        home: 'Página inicial',
        taskList: 'Lista de Tarefas',
        userList: 'Lista de Usuários'
      },
      actions: {
        send: 'Enviar',
        save: 'Salvar',
        clear: 'Limpar',
        clearAll: 'Limpar Tudo',
        restart: 'Reiniciar',
        filter: 'Filtrar',
        search: 'Pesquisar',
        list: 'Listar',
        edit: 'Editar',
        cancel: 'Cancelar',
        update: 'Atualizar',
        remove: 'Remover',
        getOut: 'Sair',
        add: 'Adicionar',
        in: 'Entrar',
        loadImage: 'Carregar Imagem',
        close: 'Fechar',
        detail: 'Detail'
      },
      fields: {
        date: 'Data',
        action: 'Ação',
        actions: 'Ações',
        project: {
          name: 'Nome',
          totalTask: 'Total de Tarefas'
        },
        task: {
          done: 'Não Feito / Feito',
          tasks: 'Tarefas'
        },
        user: {
          profiles: 'Perfis',
          nameOrEmail: 'Nome ou Email'
        }
      },
      layout: {
        menu: {
          projects: 'Projetos',
          home: 'Inicial',
          preferences: 'Preferências',
          sampleSubMenu1: 'Examplo de sub menu 1',
          sampleSubMenu2: 'Exemplo de sub menu 1'
        },
        home: {
          welcomed: 'Bem vindo à página inicial do DryPack!',
          basicPage: 'Esta é uma página básica do cliente'
        }
      },
      tooltips: {
        user: {
          perfil: 'Perfil',
          transfer: 'Transferir'
        },
        task: {
          listTask: 'Listar Tarefas'
        }
      },
      pagination: {
        total: 'Total',
        items: 'Item(s)'
      }
    })

}());
