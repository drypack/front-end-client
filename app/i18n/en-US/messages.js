/*eslint angular/file-name: 0, no-undef: 0*/
(function() {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.messages', {
      internalError: 'Internal error. contact the system admin',
      notFound: 'Not found',
      notAuthorized: 'You do not have access to this functionality.',
      searchError: 'Search error.',
      saveSuccess: 'Item saved successfully.',
      operationSuccess: 'Operation done with success.',
      operationError: 'Error while doing the operation',
      saveError: 'Error while trying to save the item.',
      removeSuccess: 'Removal done with success.',
      removeError: 'Error while trying to remove the item.',
      resourceNotFoundError: 'Item not found',
      notNullError: 'All the mandatory fields must be filled.',
      duplicatedResourceError: 'Already exists an item with these data.',
      localeSwitchedTo: 'Locale switched to {{locale}}',
      validate: {
        fieldRequired: 'The field {{field}} is mandatory.'
      },
      layout: {
        error404: 'Page no found'
      },
      login: {
        logoutInactive: 'You have been logged out. Login again.',
        invalidCredentials: 'Invalid credentials',
        unknownError: 'It was not possible to log you in. ' +
          'Try again. If the error persists, contact the system administrator.',
        userNotFound: 'User not found'
      },
      dashboard: {
        welcome: 'Welcome {{userName}}',
        description: 'Use the for navigation.'
      },
      mail: {
        mailErrors: 'An error has occurred in the following e-mails:\n',
        sendMailSuccess: 'Email sent with success!',
        sendMailError: 'It was not possible to send the e-mail.',
        passwordSendingSuccess: 'The password recover process was initiated. If the e-mail does not arrive in 10 minutes, try again.'
      },
      user: {
        userExists: 'User already exists!',
        profile: {
          updateError: 'It was not possible to update your profile.'
        }
      }
    })

}());
