/*eslint angular/file-name: 0, no-undef: 0*/
(function() {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.models', {
      user: 'User'
    })

}());
