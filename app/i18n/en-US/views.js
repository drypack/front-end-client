/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  angular
    .module('app')
    .constant('en-US.i18n.views', {
      breadcrumbs: {
        'not-authorized': 'Not authorized',
        project: 'Project',
        preferences: 'Preferences',
        home: 'Home'
      },
      titles: {
        home: 'Home page',
        taskList: 'Task list',
        userList: 'User list'
      },
      actions: {
        send: 'Send',
        save: 'Save',
        clear: 'Clear',
        clearAll: 'Clear all',
        restart: 'Restart',
        filter: 'Filter',
        search: 'Search',
        list: 'List',
        edit: 'Edit',
        cancel: 'Cancel',
        update: 'Update',
        remove: 'Remove',
        getOut: 'Exit',
        add: 'Add',
        in: 'enter',
        loadImage: 'Load image',
        close: 'Close',
        detail: 'Detail'
      },
      fields: {
        date: 'Date',
        action: 'Action',
        actions: 'Actions',
        project: {
          name: 'Name',
          totalTask: 'Total tasks'
        },
        task: {
          done: 'Not done / Done',
          tasks: 'Tasks'
        },
        user: {
          profiles: 'Profiles',
          nameOrEmail: 'Name or e-mail'
        }
      },
      layout: {
        menu: {
          projects: 'Projects',
          home: 'Home',
          preferences: 'Preferences',
          sampleSubMenu1: 'Sample sub menu 1',
          sampleSubMenu2: 'Sample sub menu 1'
        },
        home: {
          welcomed: 'Welcome to the DryPack home!',
          basicPage: 'This is a basic client page'
        }
      },
      tooltips: {
        user: {
          profile: 'Profile',
          transfer: 'Transfer'
        },
        task: {
          listTask: 'Listar Tarefas'
        }
      },
      pagination: {
        total: 'Total',
        items: 'Item(s)'
      }
    })

}());
