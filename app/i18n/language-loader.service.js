(function () {

  'use strict';

  angular
    .module('app')
    .factory('languageLoader', LanguageLoader);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function LanguageLoader($q, SupportService, $log, $injector) {
    var service = this;

    service.translate = function (locale) {
      return {
        global: $injector.get(locale + '.i18n.global'),
        views: $injector.get(locale + '.i18n.views'),
        attributes: $injector.get(locale + '.i18n.attributes'),
        dialog: $injector.get(locale + '.i18n.dialog'),
        messages: $injector.get(locale + '.i18n.messages'),
        models: $injector.get(locale + '.i18n.models')
      };
    }

    // return promise that, when resolved,
    // redefines all the translations based in web app translations
    // and remote translations that are retrieved and merged with web app translations
    return function(options) {
      $log.info('Loading the language content ' + options.key);

      var deferred = $q.defer();

      // Load the translations needed from the back-end, so it is not necessary to repeat them in the fonrt-end
      SupportService.langs().then(function(langs) {
        // Merge them with the translations defined in the front-end
        var webAppTranslations = service.translate(options.key);
        var data = angular.merge(webAppTranslations, langs);

        return deferred.resolve(data);
      }, function() {
        return deferred.resolve(service.translate(options.key));
      });

      return deferred.promise;
    }
  }

})();
