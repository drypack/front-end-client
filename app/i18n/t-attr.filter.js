(function() {

  'use strict';

  angular
    .module('app')
    .filter('tAttr', tAttr);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function tAttr($filter) {
    /**
     * Translation filter for a model attribute
     *
     * @param {any} name attribute name
     * @returns the translated attribute or the original value passed, if not found
     */
    return function(name) {
      var key = 'attributes.' + name;
      var translate = $filter('translate')(key);

      return (translate === key) ? name : translate;
    }
  }

})();
