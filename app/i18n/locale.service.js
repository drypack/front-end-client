(function() {
  'use strict';

  angular
    .module('app')
    .factory('LocaleService', LocaleService);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function LocaleService(Global, $injector, $window, $translate, moment, lodash, tmhDynamicLocale, $rootScope) { // NOSONAR

    var service = {
      getCurrentLocale: getCurrentLocale,
      setLocale: setLocale,
      isMultiLocale: isMultiLocale,
      getCurrentTimeFormat: getCurrentTimeFormat,
      getCurrentDateFormat: getCurrentDateFormat,
      getCurrentDateTimeFormat: getCurrentDateTimeFormat

    };

    /**
     * Get the current locale
     *
     * @return string locale
     */
    function getCurrentLocale() {
      var locale = localStorage.getItem('locale') || $window.navigator.language || $window.navigator.userLanguage;
      var validLocale = null;

      try {
        validLocale = $injector.get(locale + '.i18n.global');
      } catch (err) {
       // silence is gold
      }

      if (angular.isUndefined(validLocale) || validLocale === null) {
        locale = Global.defaultLocale;
      }
      return locale;
    }

    /**
     * Get the current locale object
     *
     * @returns
     */
    function getCurrentLocaleObject() {
      return lodash.find(Global.locales, function(locale) {
        return locale.id === getCurrentLocale();
      });
    }

    /**
     * Get the current locale time format
     *
     * @returns
     */
    function getCurrentTimeFormat() {
      var localeAttr = getCurrentLocaleObject();

      return localeAttr.tmFormat;

    }

     /**
     * Get the current locale date format
     *
     * @returns
     */
    function getCurrentDateFormat() {
      var localeAttr = getCurrentLocaleObject();

      return localeAttr.dtFormat;
    }

     /**
     * Get the current locale date time format
     *
     * @returns
     */
    function getCurrentDateTimeFormat() {
      return getCurrentDateFormat() + ' ' + getCurrentTimeFormat();
    }


    /**
     * Define a locale for the web application
     * redefining all the translations based in web app translations
     * and remote translations that are retrieved and merged with web app translations
     *
     * @param string locale
     * @return void
     */
    function setLocale(locale) {
      if (angular.isUndefined(locale)) { locale = getCurrentLocale(); }

      // set the locale for moment
      moment.locale(locale);

      tmhDynamicLocale.set(locale.toLowerCase());

      // it is mandatory to save the locale
      // in local storage before run/return the $translate
      localStorage.setItem('locale', locale);

      // The date/time formats are defined in the $rootScope
      // so they are accessible in the views without the controller prefix
      $rootScope.dateFormat = getCurrentDateFormat();
      $rootScope.timeFormat = getCurrentTimeFormat();
      $rootScope.dateTimeFormat = getCurrentDateTimeFormat();

      // return a promise
      return $translate.use(locale);
    }

    /**
     * Determine if the web app is multi language
     *
     * @returns
     */
    function isMultiLocale() {
      return angular.isDefined(Global.locales) && Global.locales.length > 1;
    }

    return service;
  }

}());
