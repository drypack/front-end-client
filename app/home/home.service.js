(function() {
  'use strict';

  angular
    .module('app')
    .factory('HomeService', HomeService);

  /** @ngInject */
  function HomeService(serviceFactory) {
    var model = serviceFactory('home', {
      actions: { },
      instance: { }
    });

    return model;
  }

}());
