(function() {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for the home state
   *
   * @param {object} $stateProvider
   * @param {object} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.home', {
        url: '/home',

        templateUrl: Global.clientPath + '/home/home.html',
        controller: 'HomeController as homeCtrl',
        data: { name: 'Home' }
      });
  }
}());
