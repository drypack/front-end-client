(function () {

  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function HomeController($controller, HomeService) {

    var vm = this;

    //Functions Block

    // instantiate base controller
    $controller('CRUDController', {
      vm: vm,
      modelService: HomeService,
      options: {
        searchOnInit: false
      }
    });
  }

})();
