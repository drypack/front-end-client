(function () {
  'use strict';

  angular
    .module('app')
    .config(config);

  /** @ngInject */
  // eslint-disable-next-line max-params
  function config(Global, $mdThemingProvider, $modelFactoryProvider,
    $translateProvider, $mdAriaProvider, tmhDynamicLocaleProvider) {

    $translateProvider
      .useLoader('languageLoader')
      .useSanitizeValueStrategy('escape')
      .fallbackLanguage(Global.defaultLocale);

    // set the dynamic angular locale path pattern
    // only the angular default locale js must be included @see app/app.global.js [defaultLocale],
    // because the others are loaded dynamically @see app/app.config.js [tmhDynamicLocaleProvider]
    tmhDynamicLocaleProvider.localeLocationPattern(Global.srcBasePath + '/node_modules/angular-i18n/angular-locale_{{locale}}.js');

    // the prefix to be used in all back-end api requests
    $modelFactoryProvider.defaultOptions.prefix = Global.apiPath;

    // Configuration theme - see: https://material.angularjs.org/latest/Theming/03_configuring_a_theme
    // Configuration theme
    $mdThemingProvider.theme('default')
      .primaryPalette('blue', {
        default: '700'
      })
      .accentPalette('indigo')
      .warnPalette('deep-orange');


    // Enable browser color
    $mdThemingProvider.enableBrowserColor();
    $mdAriaProvider.disableWarnings();
  }
}());
