/*eslint angular/file-name: 0, no-undef: 0*/
(function () {
  'use strict';

  // prefix used to all menu items
  // dont remove this unless you know what you are doing!
  var menuPrefix = 'views.layout.menu.';

  angular
    .module('app')
    .constant('MenuStates', [

      /*
      |--------------------------------------------------------------------------
      | Front-end application menu definition
      |--------------------------------------------------------------------------
      |
      | Put here the menu items and subitems of the application using the examples
      | You can add/change all the native menu items, but then the corresponding functionalities
      | will not be accessible in the front-end
      */

      // home, the default state after logging in
      { state: 'app.home', title: menuPrefix + 'home', icon: 'home', subItems: [] },

      // other menu items
      { state: 'app.project', title: menuPrefix + 'projects', icon: 'donut_large', subItems: [] },
      { state: '#', title: menuPrefix + 'preferences', icon: 'star', subItems: [
          { state: '#', title: menuPrefix + 'sampleSubMenu1', icon: 'people' },
          { state: '#', title: menuPrefix + 'sampleSubMenu2', icon: 'mail' }
      ]
      }
    ]);
}());
