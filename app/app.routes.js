(function () {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /** @ngInject */
  function routes($stateProvider, $urlRouterProvider, Global) {
    $stateProvider
      .state('app', {
        templateUrl: Global.clientPath + '/shared/partials/app.html',
        abstract: true,
        resolve: { // ensure langs is ready before render view
          translateReady: ['$injector', '$q', function ($injector) {
            // return a promise
            return $injector.get('LocaleService').setLocale();
          }]
        }
      })

    $urlRouterProvider.when('/app', Global.homeUrl);
    $urlRouterProvider.otherwise(Global.homeUrl);
  }
}());
