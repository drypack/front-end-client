(function () {
  'use strict';

  angular
    .module('app')
    .constant('Global', {
      appName: 'ACME',
      homeState: 'app.home',
      homeUrl: '/home',
      apiPath: 'api/v1',
      srcBasePath: 'client',
      clientPath: 'client/app/',
      appLayout: 'headermenu', //'sidemenu'
      imagePath: 'client/images',
      locales: [
        { id: 'pt-BR', dtFormat: 'DD/MM/YYYY', tmFormat: 'HH:mm' },
        { id: 'en-US', dtFormat: 'YYYY/MM/DD', tmFormat: 'HH:mm' }
      ],
      defaultLocale: 'en-US'
    });
}());
