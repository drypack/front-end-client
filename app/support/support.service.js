(function() {
  'use strict';

  angular
    .module('app')
    .factory('SupportService', SupportService);

  /** @ngInject */
  function SupportService(serviceFactory) {
    return serviceFactory('support', {
      actions: {
      /**
       * Get the translations from the back-end
       *
       * @returns {promise}
       */
        langs: {
          method: 'GET',
          url: 'langs',
          wrap: false,
          cache: false
        }
      }
    });
  }

}());
