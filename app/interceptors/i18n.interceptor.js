(function () {
  'use strict';

  angular
    .module('app')
    .config(i18nInterceptor);

  function i18nInterceptor($httpProvider, $provide) {
    /**
     * This interceptor is responsible for showing the
     * http warning header or add it as pending to be show later
     *
     * @param {any} $q
     * @param {any} $injector
     * @returns
     */
    function setLocale($injector) {
      return {
        request: function (config) {
          var win = $injector.get('$window'); 
          var locale = localStorage.getItem('locale') || win.navigator.language || win.navigator.userLanguage;

          config.headers['Locale'] = locale;
          return config;;
        }
      };
    }

    // Define uma factory para o $httpInterceptor
    $provide.factory('setLocale', setLocale);

    // Push the new factory onto the $http interceptor array
    $httpProvider.interceptors.push('setLocale');
  }
}());
