(function () {
  'use strict';

  angular
    .module('app')
    .config(exceptionInterceptor);

  function exceptionInterceptor($httpProvider, $provide) {
    /**
     * This interceptor is responsible for showing the
     * error messages referring to the back-end validations
     *
     * @param {any} $q
     * @param {any} $injector
     * @returns
     */
    function showError($q, $injector) {
      return {
        responseError: function (rejection) {
          var C2Toast = $injector.get('C2Toast');
          var $translate = $injector.get('$translate');

          if (angular.isDefined(rejection.data) && angular.isDefined(rejection.data.error)) {
            var languageKey = rejection.data.error;

            C2Toast.error($translate.instant(languageKey));
            return $q.reject(rejection);
          }

          if (rejection.config.data && !rejection.config.data.skipValidation) {
            if (rejection.data && rejection.data.error) {

              // Verifies if any token error has ocurred
              if (rejection.data.error.startsWith('token_')) {
                C2Toast.warn($translate.instant('messages.login.logoutInactive'));
              } else {
                // In most cases here the rejection.status is 403 "Forbidden"
                // because of lack of permission to the action
                C2Toast.error($translate.instant(rejection.data.error));
              }
            } else {
              C2Toast.errorValidation(rejection.data);
            }
          }

          return $q.reject(rejection);
        }
      };
    }

    // Define a factory to the $httpInterceptor
    $provide.factory('showError', showError);

    // Push the new factory onto the $http interceptor array
    $httpProvider.interceptors.push('showError');
  }
}());
