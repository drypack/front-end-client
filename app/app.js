/*eslint angular/file-name: 0*/
(function () {
  'use strict';

  angular.module('app', [
    'ngAnimate',
    'ngAria',
    'ui.router',
    'ngC2',
    'ui.utils.masks',
    'text-mask',
    'ngMaterial',
    'modelFactory',
    'md.data.table',
    'ngMaterialDatePicker',
    'pascalprecht.translate',
    'angularFileUpload',
    'tmh.dynamicLocale'
  ]);
})();
