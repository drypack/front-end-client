(function () {

  'use strict';

  angular
    .module('app')
    .controller('AppController', AppController);

  /** @ngInject */
  /**
   * Controller responsible for functionalities that are used in any system state/page
   */
  /** @ngInject */
  // eslint-disable-next-line max-params
  function AppController(Global, $mdMedia, LocaleService, $translate, C2Toast, $state) {

    var vm = this;

    vm.isSideMenuLayout = isSideMenuLayout;
    vm.isHeaderMenuLayout = isHeaderMenuLayout;
    vm.changeLocale = changeLocale;
    vm.isMultiLocale = isMultiLocale;

    // Current year to be shown in the footer
    vm.currentYear = null;
    activate();

    function activate() {
      var date = new Date();

      vm.currentYear = date.getFullYear();
      vm.locale = LocaleService.getCurrentLocale();
    }

    function isSideMenuLayout() {
      var layoutGtSm = $mdMedia('gt-sm');

      return !layoutGtSm || Global.appLayout === 'sidemenu';
    }

    function isHeaderMenuLayout() {
      var layoutGtSm = $mdMedia('gt-sm');

      return layoutGtSm && Global.appLayout === 'headermenu';
    }

    /**
     * Change the locale based in the vm.locale model value
     * redefining all the translations based in web app translations
     * and remote translations that are retrieved and merged with web app translations
     *
     * @return void
     */
    function changeLocale(locale) {
      // add the set locale promise
      if (angular.isDefined(locale)) {
        vm.locale = locale.id || locale;
      }
      LocaleService.setLocale(vm.locale).then(function () {
        $state.reload();
        C2Toast.info($translate.instant('messages.localeSwitchedTo', {
          locale: vm.locale.toUpperCase()
        }));
      });
    }

    /**
     * Determine if the web app is multi language
     *
     * @returns
     */
    function isMultiLocale() {
      return LocaleService.isMultiLocale();
    }
  }

})();
