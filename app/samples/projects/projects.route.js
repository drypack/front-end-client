(function () {
  'use strict';

  angular
    .module('app')
    .config(routes);

  /**
   * Configure the route for project
   *
   * @param {any} $stateProvider
   * @param {any} Global
   */
  /** @ngInject */
  function routes($stateProvider, Global) {
    $stateProvider
      .state('app.project', {
        url: '/projects',
        templateUrl: Global.clientPath + '/samples/projects/projects.html',
        controller: 'ProjectsController as projectsCtrl',
        data: {
          needAuthentication: true,
          needPermission: {
            resource: 'project'
          }
        }
      });
  }
}());
